﻿using System;
using System.Collections;
using System.Collections.Generic;

public class FixedNode<T> : INode<T>
{
    public int Id{get; private set;}

    public T Value{get; private set;}

    public FixedNode(int id, T possibleValue){
        Id = id;
        Value = possibleValue;
    }
}
