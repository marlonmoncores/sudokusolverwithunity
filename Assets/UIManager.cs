﻿using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject sudokuFieldsHolder;
    public InputField sudokuInput;
    public Slider speedInput;
    public Dropdown stepSize;
    public GameObject solveBtn;
    public Text solutionStatus;
    
    
    public Font font;

    private SudokuResolver _resolver;

    private Dictionary<int,Text> _texts = new Dictionary<int,Text>();


    private bool _solving = false;

    private int _currentSpeed;

    private int _currentStepSize;

    private static int TOTAL_FIELDS=81;


    void Start()
    {
        InitGUI();
    }
    private IEnumerator Solve(){
        while(_solving){
           _resolver.RunNextStep(_currentStepSize);
           yield return new WaitForSeconds(1f/_currentSpeed);
        }
    }

    private void InitGUI(){
        for(int i=0;i<TOTAL_FIELDS;i++){
            CreateGameObjectToTrackNodeValue(i);
        }
        
        solutionStatus.text = "";
        
        Button btn = solveBtn.GetComponent<Button>();
        solveBtn.GetComponentInChildren<Text>().text = "Solve";
		btn.onClick.AddListener(()=> {
            if(!_solving){
                OnStart();
            }else{
                _resolver.Abort();
            }
        } );
        
        speedInput.onValueChanged.AddListener((speed) => _currentSpeed = (int)speed);
        _currentSpeed = (int)speedInput.value;
        
        sudokuInput.onValueChanged.AddListener((value) =>{
            sudokuInput.text = new Regex(@"[^\d]").Replace(value,"");
            parseInputSudokuField(value,'?');
        });
        
        ConfigureStepSize(stepSize.value);
        stepSize.onValueChanged.AddListener((value) => ConfigureStepSize(value));
    }

    private void ConfigureStepSize(int selectedOption){
        int value = 1;
        for (int i=0;i<selectedOption;i++){
            value *= 10;
        }
        _currentStepSize = value;
    }

    private void parseInputSudokuField(string value, char defaulChar){
        var chars = value.ToCharArray();
        for ( int i =0;i<TOTAL_FIELDS;i++){
            char currentValue = ' ';
            if(i<value.Length){
                currentValue = Char.GetNumericValue(chars[i]) > 0 ? chars[i] : defaulChar;
            }
            OnNodeValueChanged(i, currentValue+"", currentValue != defaulChar);            
        }
    }

    private void OnNodeValueChanged(int nodeId, string newValue, bool isFixedValue){
        UpdateText(nodeId, newValue, isFixedValue);
        //DebugInfo(nodeId, newValue);
    }

    private void OnStart(){
        _solving=true;
        parseInputSudokuField(sudokuInput.text,' ');
        _resolver = SudokuResolver.BuildResolver(sudokuInput.text);
        _resolver.NodeValueChangedEvent += (id, value, f) => OnNodeValueChanged(id, (value == 0 ? "" : value+""), f);
        _resolver.FinishedEvent += OnFinish;
        solutionStatus.text = "";
        sudokuInput.interactable = false;
        solveBtn.GetComponentInChildren<Text>().text = "Abort";
        StartCoroutine("Solve");
    }

    private void OnFinish(SudokuResolver.status status){
        _solving = false;
        sudokuInput.interactable = true;
        solveBtn.GetComponentInChildren<Text>().text = "Solve";
        if(status == SudokuResolver.status.SOLUTION_FOUND){
            solutionStatus.text = "FINISHED!";
            solutionStatus.color = Color.blue;
        }else if(status ==SudokuResolver.status.IMPOSSIBLE_TO_FIND){
            solutionStatus.text = "SOLUTION NOT POSSIBLE!";
            solutionStatus.color = Color.red;
        }else{
            solutionStatus.text = "CANCELED BY USER";
            solutionStatus.color = Color.red;
        }
        
    }
    private void CreateGameObjectToTrackNodeValue(int nodeId){
        GameObject gameObject = new GameObject("Text");
        gameObject.transform.SetParent(sudokuFieldsHolder.transform);
        
        RectTransform trans = gameObject.AddComponent<RectTransform>();
        trans.pivot = new Vector2(1,0);
        float multiplyer = 79.25f;
        float offsetX = 15;
        float offsetY = -30;
        int nodeRow = nodeId / 9; 
        int nodeCol = nodeId % 9;
        trans.localPosition = new Vector3((multiplyer * nodeCol)+offsetX, (multiplyer * nodeRow*-1)+offsetY, 0);
        trans.sizeDelta = new Vector2(70,70);
        trans.localScale = new Vector3(1,1,1);
        
        Text text = gameObject.AddComponent<Text>();
        text.text = "";
        text.fontSize = 40;
        text.color = Color.black;
        text.alignment = TextAnchor.MiddleCenter;
        text.font = font;
        _texts.Add(nodeId, text);
    }

    private void UpdateText(int nodeId, string newValue, bool isFixedValue){
        _texts[nodeId].text = newValue+"";
        _texts[nodeId].color = isFixedValue ? Color.black : Color.red;
    }

    private void DebugInfo(int nodeId, int nodeValue){
        Debug.Log("Node "+nodeId+ " has changed its value to "+nodeValue);
    }

    private void LogCurrentSolution(){
        Debug.Log("Current solution: "+ _resolver.CurrentSolution());
    }
}
