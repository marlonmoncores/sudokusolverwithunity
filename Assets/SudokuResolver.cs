﻿using System;
using System.Collections;
using System.Collections.Generic;

public class SudokuResolver
{
    private INode<int>[] _nodes;

    private BreadthFirstSearchEventBased<int> _graphSolver;

    private static int TOTAL_NODES = 81;
    
    private Dictionary<int,List<INode<int>>> _rowsElements = new Dictionary<int,List<INode<int>>>();
    private Dictionary<int,List<INode<int>>> _colsElements = new Dictionary<int,List<INode<int>>>();
    private Dictionary<int,List<INode<int>>> _squareElements = new Dictionary<int,List<INode<int>>>();

    public Action<int, int, bool> NodeValueChangedEvent;

    public Action<status> FinishedEvent;

    public enum status {SOLUTION_FOUND, IMPOSSIBLE_TO_FIND, CANCELED};

    private SudokuResolver(){
    }


    public static SudokuResolver BuildResolver(string elements){
        if(elements.Length > TOTAL_NODES){
            throw new Exception("Sudoku text can't have more than 81 characters length");
        }

        if(elements.Length < TOTAL_NODES){
            while(elements.Length < TOTAL_NODES){
                elements = elements+= "-";
            }
        }
        SudokuResolver resolver = new SudokuResolver();

        resolver._graphSolver = new BreadthFirstSearchEventBased<int>(resolver.CheckRulesByNodeModification);
        resolver.CreateNodes(elements);
        resolver._graphSolver.NodeValueChangedEvent += resolver.OnNodeValueChanged;

        return resolver;
    }

    public void Abort(){
        _graphSolver.NodeValueChangedEvent -= OnNodeValueChanged;
        _graphSolver = null;
        if(FinishedEvent != null){
            FinishedEvent(status.CANCELED);
        }
    }

    public bool RunNextStep(int quantity = 1){
        for(int i=0;i<quantity;i++){
            if(!_graphSolver.RunNextStep()){
                PublishFinishedEvent();
                return false;
            }
        }
        return true;
    }

    private void OnNodeValueChanged(INode<int> node){
        if(NodeValueChangedEvent != null){
            NodeValueChangedEvent(node.Id, node.Value, _nodes[node.Id] is FixedNode<int>);
        }
    }

    private void CreateNodes(string elements, Action<int,int,bool> onNodeCreatedCallback = null){
        _nodes = new INode<int>[TOTAL_NODES];
        int index = 0;
        INode<int> lastAddedNode = null;
        foreach (char current in elements.ToCharArray()){
            int readValue = (int)Char.GetNumericValue(current);
            if(readValue > 0){
                _nodes[index] = new FixedNode<int>(index, readValue);
            }else{
                _nodes[index] = _graphSolver.CreateNode(index, new int[]{1,2,3,4,5,6,7,8,9}, lastAddedNode);
                lastAddedNode = _nodes[index];
            }

            SaveCreatedNodeOnDictionaries(_nodes[index]);
            index++;
        }
    }

    private void SaveCreatedNodeOnDictionaries(INode<int> node){
        int nodeRow = node.Id / 9;
        int nodeCol = node.Id % 9;
        var nodeSquare = GetNodeSquare(nodeRow, nodeCol);

        AddOnDictionaryList(_rowsElements, nodeRow, node);
        AddOnDictionaryList(_colsElements, nodeCol, node);
        AddOnDictionaryList(_squareElements, nodeSquare, node);
    }

    private void AddOnDictionaryList(Dictionary<int,List<INode<int>>> dict, int key, INode<int> node){
        if(!dict.ContainsKey(key)){
            dict.Add(key, new List<INode<int>>());
        }
        dict[key].Add(node);
    }

    private int GetNodeSquare(int nodeRow, int nodeCol){
        return ((int)(nodeRow/3))*3 + ((int)(nodeCol/3)); 
    }

    private bool CheckRulesByNodeModification(INode<int> node){
        int nodeRow = node.Id / 9;
        int nodeCol = node.Id % 9;
        var nodeSquare = GetNodeSquare(nodeRow, nodeCol);
        
        bool reject = 
            CheckCollisionWithOthersOccurs(_rowsElements, nodeRow, node)
            ||
            CheckCollisionWithOthersOccurs(_colsElements, nodeCol, node)
            ||
            CheckCollisionWithOthersOccurs(_squareElements, nodeSquare, node)
            ;
        
        return !reject;
    }

    private bool CheckCollisionWithOthersOccurs(Dictionary<int,List<INode<int>>> dict,int key, INode<int> node){
        return dict[key].Exists((current) => current.Id != node.Id && current.Value > 0 && current.Value == node.Value);
    }

    public string CurrentSolution(){
        string solution = "";
        foreach(INode<int> node in _nodes ){
            solution += node.Value;
        }
        return solution;
    }

    private void PublishFinishedEvent(){
        _graphSolver.NodeValueChangedEvent -= OnNodeValueChanged;
        _graphSolver = null;
        if(FinishedEvent != null){
            FinishedEvent(CheckSolutionIsComplete() ? status.SOLUTION_FOUND: status.IMPOSSIBLE_TO_FIND);
        }
    }

    private bool CheckSolutionIsComplete(){
        return !CurrentSolution().Contains("0");
    }
}
