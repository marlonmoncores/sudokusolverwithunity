﻿using System;
using System.Collections;
using System.Collections.Generic;

public class BackTrackableNode<T> : INode<T>
{
    
    public Action<BackTrackableNode<T>,bool> OnReset;

    public Action<BackTrackableNode<T>> OnChangeValue;

    public int Id{get; private set;}

    public T Value{get{return _index != -1 ? _possibleValues[_index] : default(T);}}

    private T[] _possibleValues;
    private int _index;

    public BackTrackableNode(int id, T[] possibleValues){
        Id = id;
        _possibleValues = possibleValues;
        _index = -1;
    }

    
    public void NextValue(){
        _index++;
        if(_index < _possibleValues.Length){
            if(OnChangeValue != null){
                OnChangeValue(this);
            }
        }else{
            Reset();
        }
    }

    public void Reset(bool fromParent = false){
        if (_index == -1){
            return;
        }
        _index = -1;
        if(OnReset !=null){
            OnReset(this, fromParent);
        }
    }
}
