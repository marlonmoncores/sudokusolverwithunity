﻿using System;
using System.Collections;
using System.Collections.Generic;

public class BreadthFirstSearchEventBased<T>
{
    private class NodeChildInfo{
        public List<BackTrackableNode<T>> children = new List<BackTrackableNode<T>>();
        public int currentChild = 0;
    }

    public Action<INode<T>> NodeValueChangedEvent;

    private BackTrackableNode<T> _nextNodeToChangeValue;

    private Func<BackTrackableNode<T>,bool> _isSolutionPossible;

    private Dictionary<BackTrackableNode<T>, NodeChildInfo> _treeInfo = new Dictionary<BackTrackableNode<T>, NodeChildInfo>();
    
    public BreadthFirstSearchEventBased(Func<BackTrackableNode<T>, bool> _isSolutionPossibleChecker){
        _isSolutionPossible = _isSolutionPossibleChecker;
    }

    public INode<T> CreateNode(int nodeId, T[] possibleValues, INode<T> parentNode = null){
        BackTrackableNode<T> node = new BackTrackableNode<T>(nodeId,possibleValues);
        node.OnReset += (n,fromParent) => OnNodeReset(n);
        node.OnChangeValue += OnNodeChangeValue;
        
        if(parentNode != null && parentNode is BackTrackableNode<T>){
            BackTrackableNode<T> parent = (BackTrackableNode<T>)parentNode;
            parent.OnReset += (n,fromParent) => node.Reset(true);
            node.OnReset += (n,fromParent) => OnCurrentChildReseted(parent,fromParent);
            NodeChildInfo nodeChildInfo;
            if (! _treeInfo.TryGetValue(parent, out nodeChildInfo)){
                nodeChildInfo = new NodeChildInfo();
                _treeInfo.Add(parent, nodeChildInfo);   
            }
            nodeChildInfo.children.Add(node);    
        }else{
            SetNextNode(node);
        }
        return node;
    }
    
    public bool RunNextStep(){
        if(_nextNodeToChangeValue != null){
            BackTrackableNode<T> next = _nextNodeToChangeValue;
            _nextNodeToChangeValue = null;
            next.NextValue();
            return true;
        }
        return false;
    }

    private void OnNodeChangeValue(BackTrackableNode<T> node){
        NotifyNodeChangedValue(node);
        if(!_isSolutionPossible(node))
        {
            SetNextNode(node);
            return;
        }

        NodeChildInfo nodeChildInfo;
        if (_treeInfo.TryGetValue(node, out nodeChildInfo)){
            if(nodeChildInfo.currentChild < nodeChildInfo.children.Count){
                SetNextNode(nodeChildInfo.children[nodeChildInfo.currentChild++]);
            }
        }
    }

    private void OnNodeReset(BackTrackableNode<T> node){
        NodeValueChangedEvent(node);
        NodeChildInfo nodeChildInfo;
        if (_treeInfo.TryGetValue(node, out nodeChildInfo)){
            nodeChildInfo.currentChild = 0;
        }
    }

    private void OnCurrentChildReseted(BackTrackableNode<T> node, bool fromParent){
        if(!fromParent){
            _treeInfo[node].currentChild --;
            SetNextNode(node);
        }
    }

    private void SetNextNode(BackTrackableNode<T> node){
        if (_nextNodeToChangeValue ==null){
            _nextNodeToChangeValue = node;
        }
    }

    private void NotifyNodeChangedValue(INode<T> node){
        if(NodeValueChangedEvent != null){
            NodeValueChangedEvent(node);
        }
    }
    
    

    

    
}
