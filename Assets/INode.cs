﻿using System;
using System.Collections;
using System.Collections.Generic;

public interface INode<T>
{
    
    int Id{get;}

    T Value{get;}    
}
